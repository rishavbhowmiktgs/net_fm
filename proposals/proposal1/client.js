class Request {
  static int32_to_buffer(size){
    const buffer = Buffer.alloc(4)
    for(var i=0; i<4; i++){
      buffer[i] = size%256; size/=256
    }
    return buffer
  }
  static buffer_to_int32(buffer){
    var number = 0
    for(var i=0; i<4; i++){
      number += Number(buffer[i]) * (256**i)
    }
    return number
  }

  static introspect_to_buffer(content){
    if((content instanceof Array)||(content instanceof Buffer)||(content instanceof Uint8Array)) return Buffer.concat([Buffer.from('b'), Buffer.from(content)])
    if(typeof content == 'string') return Buffer.concat([Buffer.from('s'), Buffer.from(content)])
    if(typeof content == 'object') return Buffer.concat([Buffer.from('o'), Buffer.from(JSON.stringify(content))])
    throw Error("Invalid Content")
  }

  static introspect_from_buffer(buffer){
    if(buffer.length<1) throw Error("Invalid Buffer")
    const type = String(buffer.slice(0,1))
    if(type == 'b') return buffer.slice(1, buffer.length)
    if(type == 's') return String(buffer.slice(1, buffer.length))
    if(type == 'o') return JSON.parse(String(buffer.slice(1, buffer.length)))
  }

  static capsule(h, b){
    const header = Request.introspect_to_buffer(h)
    const header_size = Request.int32_to_buffer(header.length)
    const body = Request.introspect_to_buffer(b)
    const body_size = Request.int32_to_buffer(body.length)
    return Buffer.concat([header_size, body_size, header, body])
  }

  static antonym(buffer, callback=null){
    try{
      const header_size = Request.buffer_to_int32(buffer.slice(0,4)),
            body_size = Request.buffer_to_int32(buffer.slice(4,8))
      const header = Request.introspect_from_buffer(buffer.slice(8,8+header_size)),
            body = Request.introspect_from_buffer(buffer.slice(8+header_size, 8+header_size+body_size))
      if(callback) callback(null, header, body) //error, header object, body
      return {header:header, body:body}
    }catch(e){
      if(callback) callback(e, null, null) //error, header object, body
      return {error:e, header:null, body:null}
    }
  }
}


class TCPClient {
  constructor(options, callback=null) {
    const this_class = this
    try {
      this_class.data_inflow_event_list = {}

      this_class.socket = TCPClient.net.createConnection(options, () => {
        if(typeof callback == 'function') callback(this_class)
      })

      this_class.socket.on('data', (data)=>{
        try {
          const content = Request.antonym(data)
          const id = content.header.id
          console.log(70, id);
          if((typeof id != 'string')&&(typeof id != 'number')) throw Error('bad server call')
          if(this_class.data_inflow_event_list[id]){
            this_class.data_inflow_event_list[id](content.body)
          }
        } catch (e) {
          console.log(75, e);
        }
      })

      this_class.socket.on('end', () => {
        console.log('disconnected')
      })

    } catch (e) {
      throw Error('failed to connect\n'+e)
    }
  }
  emit(id, content){
    const this_class = this
    this_class.socket.write(Request.capsule({id:id}, content))
  }
  on(id, function_call){
    if(typeof function_call != 'function') throw Error("2nd Parameter should be function")
    this.data_inflow_event_list[id] = function_call
  }
  end(){
    this.socket.end()
  }
}
TCPClient.net = require('net')

//test
const cli = new TCPClient({port:8800}, (socket)=>{
  cli.on('id1', (data)=>{
    console.log(`Server Says - ${data.toString()}`)
    socket.emit('id1', "Yes we are connect :-)")
  })
  //console.log(cli.data_inflow_event_list);
})
//console.log(Request.antonym(Request.capsule({id:12}, ("Hello World"))))
