# All discovered and un-resolved bugs

## Error: read ECONNRESET
#### About it
This error is throw by both client and server side script in even of unhandled network shutdown in side of connected stackholder.

Which means of if client side script is stopped **without** proper connection end **read ECONNRESET** error is throw.

Also the orgin of this error is **yet to be discovered**, for now the error stays unhandeled and results in the termination of the whole server.

**Identifed** that the error is originted from 'net' module itself and not caused by the frame work.

#### Full Error Log
**server.js**
        
    events.js:292
      throw er; // Unhandled 'error' event
    
    Error: read ECONNRESET
    at TCP.onStreamRead (internal/stream_base_commons.js:205:27)
    Emitted 'error' event on Socket instance at:
    at emitErrorNT (internal/streams/destroy.js:92:8)
    at emitErrorAndCloseNT (internal/streams/destroy.js:60:3)
    at processTicksAndRejections (internal/process/task_queues.js:84:21) {
      errno: 'ECONNRESET',
      code: 'ECONNRESET',
      syscall: 'read'
    }
    
**client.js**

    events.js:292
      throw er; // Unhandled 'error' event
      ^

    Error: read ECONNRESET
        at TCP.onStreamRead (internal/stream_base_commons.js:205:27)
    Emitted 'error' event on Socket instance at:
        at emitErrorNT (internal/streams/destroy.js:92:8)
        at emitErrorAndCloseNT (internal/streams/destroy.js:60:3)
        at processTicksAndRejections (internal/process/task_queues.js:84:21) {
      errno: 'ECONNRESET',
      code: 'ECONNRESET',
      syscall: 'read'
    }

#### Possible Solution
Execute 'net' inside a worker thread : But Worker threads can not send data periodically 


## Partialy observed - rigorous use of emit leading to drop data

The issue is not clear but for now it is recomended to make proper mechanism to ensure handling og such fails. With techniques like appriciation of the other party


## "Error: This socket has been ended by the other party"
This error is thrown and crashes the whole program if emit is used after connection is borken

However this error is also the key to make a disconnect even listener

#### Full Error Log

```
Error: This socket has been ended by the other party
    at Socket.writeAfterFIN [as write] (net.js:455:14)
    at TCPClient.emit (\node_tcp\src\client.js:58:25)
    at Timeout._onTimeout (\node_tcp\examples\simple_hello_hi\clients.js:57:18)
    at listOnTimeout (internal/timers.js:554:17)
    at processTimers (internal/timers.js:497:7)
Emitted 'error' event on Socket instance at:
    at emitErrorNT (net.js:1344:8)
    at processTicksAndRejections (internal/process/task_queues.js:80:21) {
  code: 'EPIPE'
}
```