# TCP Framework (Proposal 1)
Built using 'net' module, and allows socket.io like experience

Visit Reports of bugs here : [Bug Backlogs](https://bitbucket.org/rishavbhowmiktgs/net_fm/src/6c6bbee56a6c/proposals/proposal1/bug_backlogs.md)

# Components

## Request class
This class more like a namespace, it comprises of static functions dedicated to encapsulate and decapsulate data packets.

### Significant functions in Request class
#### capsule - This class intakes header data and body and encapsulate them.

Parameters:

- `h` <string> | <object> | <Buffer> | <Uint8Array>

- `b` <string> | <object> | <Buffer> | <Uint8Array>

Return: <Buffer>

#### antonym - This class intakes header data and body and decapsulate them.

Parameters:

- `buffer` <Buffer>

Return:

{

- `error` <Error>

- `header` <string> | <object> | <Buffer>

- `body` <string> | <object> | <Buffer>
	
}

**Note** : In functions capsule <Uint8Array> is always converted to <Buffer>

### Format of encapsulate data
Encapsulated data is Buffer data, with very specific number of bytes in beging for very specific data.
The length of Buffer in encapsulate data can vary from **10 bytes** to **2X256^4 + 8 bytes**

This buffer follow the following sequence

| Name                | type                             | size (bytes) |   |   |
|---------------------|----------------------------------|--------------|---|---|
| header_size         | Unsigned 4 byte Integer (number) | 4            |   |   |
| body_size           | Unsigned 4 byte Integer (number) | 4            |   |   |
| header_content_type | 1byte char (String of length 1)  | 1            |   |   |
| header_content      | Buffer                           | 0 to 256^4   |   |   |
| body_content_type   | 1byte char (String of length 1)  | 1            |   |   |
| body_content        | Buffer                           | 0 to 256^4   |   |   |